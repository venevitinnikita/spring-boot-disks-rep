package com.home.disks;

public enum DiskContentType {
    MOVIE, GAME, MUSIC
}