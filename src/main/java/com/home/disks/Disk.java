package com.home.disks;

public class Disk {
    private String id;
    private DiskType diskType;
    private DiskContentType contentType;
    private String label;
    private String description;

    public Disk(String id, DiskType diskType, DiskContentType contentType, String label) {
        this.id = id;
        this.diskType = diskType;
        this.contentType = contentType;
        this.label = label;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DiskType getDiskType() {
        return diskType;
    }

    public void setDiskType(DiskType diskType) {
        this.diskType = diskType;
    }

    public DiskContentType getContentType() {
        return contentType;
    }

    public void setContentType(DiskContentType contentType) {
        this.contentType = contentType;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}