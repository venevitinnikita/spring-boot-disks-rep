package com.home.disks;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DisksController {
    private static final String JSP_NAME = "welcome";

    private final DisksTestStorage storage = new DisksTestStorage();
    private List<Disk> disks;

    @RequestMapping("/")
    public String welcome(Map<String, Object> model) {
        model.put("allDisks", loadAllDisks());

        return JSP_NAME;
    }

    @RequestMapping("/modify-storage")
    public String addDisk(Map<String, Object> model, @RequestParam(value = "action", required = true) String action,
            @RequestParam(value = "caption", required = true) String caption,
            @RequestParam(value = "disk_type", required = true) String diskType) {

        switch (action) {
        case "Add":
            switch (diskType) {
            case "DVD":
                addMovie(caption);
                break;
            case "CD":
                addMusic(caption);
                break;
            }
            break;
        case "Delete":
            deleteMovie(caption);
            break;
        }

        model.put("allDisks", loadAllDisks());

        return JSP_NAME;
    }

    private List<Disk> loadAllDisks() {
        return storage.loadDisks();
    }

    private void addMovie(String caption) {
        storage.addDisk(new MovieDisk(UniqueIdGenerator.newId(), caption));
    }

    private void addMusic(String caption) {
        storage.addDisk(new Disk(UniqueIdGenerator.newId(), DiskType.CD, DiskContentType.MUSIC, caption));
    }

    private void deleteMovie(String caption) {
        storage.removeByCaption(caption);
    }

    @RequestMapping("/filter")
    public String filter(Map<String, Object> model,
            @RequestParam(value = "search_text", required = true) String searchText) {

        List<Disk> disks = filtered(searchText);
        model.put("allDisks", disks);

        return JSP_NAME;
    }

    private List<Disk> filtered(String searchText) {
        if (searchText == null) {
            return storage.loadDisks();
        }

        searchText = searchText.toLowerCase();

        List<Disk> result = new ArrayList<>();
        for (Disk disk : storage.loadDisks()) {
            if (disk.getLabel().toLowerCase().contains(searchText)) {
                result.add(disk);
            }
        }

        return result;
    }
}