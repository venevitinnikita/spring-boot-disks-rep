package com.home.disks;

public class UniqueIdGenerator {
    private static long id;

    public static String newId() {
        return String.valueOf(id++);
    }
}