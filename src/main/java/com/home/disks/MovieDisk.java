package com.home.disks;

public class MovieDisk extends Disk {
    public MovieDisk(String id, String label) {
        super(id, DiskType.DVD, DiskContentType.MOVIE, label);
    }
}