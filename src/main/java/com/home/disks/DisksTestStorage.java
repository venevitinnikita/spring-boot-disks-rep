package com.home.disks;

import java.util.ArrayList;
import java.util.List;

public class DisksTestStorage {
    private final List<Disk> disks = new ArrayList<>();

    public DisksTestStorage() {
        disks.add(new MovieDisk(UniqueIdGenerator.newId(), "Пролетая над гнездом кукушки"));
        disks.add(new MovieDisk(UniqueIdGenerator.newId(), "Побег из Шоушенка"));
    }

    public List<Disk> loadDisks() {
        return disks;
    }

    public void addDisk(Disk disk) {
        disks.add(disk);
    }

    public void removeByCaption(String caption) {
        disks.removeIf(disk -> disk.getLabel().equals(caption));
    }
}