<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
	<meta charset="utf-8" />

	<c:url value="/css/main.css" var="jstlCss" />
	<link href="${jstlCss}" rel="stylesheet" />
</head>
<body>

	<%@ page contentType="text/html; charset=UTF-8" %>
	<%@ page import="java.util.List, com.home.disks.Disk" %>

	<form action="${pageContext.request.contextPath}/filter" method="post" class="filter">
		<input type="text" name="search_text" />
		<button type="submit">Поиск</button>
	</form>

	<form action="${pageContext.request.contextPath}/modify-storage" method="post" class="controls">
		<input type="text" name="caption" required />
		<button type="submit" name="action" value="Add">Добавить</button>
		<button type="submit" name="action" value="Delete">Удалить</button>
		<select name="disk_type">
			<option value="DVD">DVD</option>
			<option value="CD">CD</option>
		</select>
	</form>

	<table class="disks-table" class="table">
		<% for (Disk disk : (List<Disk>) request.getAttribute("allDisks")) { %>
			<tr>
				<td>
					<%= disk.getLabel() %>
				</td>
				<td>
					<%= disk.getDiskType().toString() %>
				</td>
			</tr>
		<% } %>
	</table>

</body>

</html>